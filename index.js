var express = require('express');
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var MongoClient = require('mongodb').MongoClient;
const { v4: uuidv4 } = require('uuid');

var app = express();

//Allow all requests from all domains & localhost
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// on verifie si le compte est bien vrai, et que le token n'est pas de la merde
// on sait jamais
async function is_google_token_valid(req, res, next) {
    let token = req.body["id"]
    let url = "https://oauth2.googleapis.com/tokeninfo?id_token=" + token
    let test = await (await fetch(url)).json()
    if (test.email) {
        next()
    } else {
        res.json({status:"non_log", text: "mon coco, evite de niquer mon site sans raison, d'ailleurs pourquoi tu fais ca ? arrete vite fais pls, je log tout"})
    }
}

MongoClient.connect("mongodb://localhost:27017/lamoulie").then(client => {
    const lamoulie = client.db("lamoulie").collection("lamoulie")
    const vote = client.db("lamoulie").collection("vote")

    app.post("/", is_google_token_valid, async function(req, res) {
        // le mec est bien sur un compte google de l'eisti, il peut ajouter a la bdd
        let lam = req.body["lamoulie"]
        let google_profile = req.body["gprof"]
        
        let count_user = await lamoulie.find({"gprof.googleId": google_profile["googleId"]}).count()

        if (count_user < 5) {
            let uuid = uuidv4()
            lamoulie.insertOne({
                title: lam,
                id: uuid,
                gprof: google_profile
            }).then(e => {
                vote.insertOne({
                    vote_id: uuid,
                    gprof: google_profile,
                    vote: 1
                }).then(() => {
                    res.json({status: "ok"})
                    console.log("user inserted")
                })

            })
        } else {
            res.json({status: "too_many"})
        }
    })

    app.post("/vote", is_google_token_valid, async function(req, res) {
        let lamoulie_id = req.body["vote_id"]
        let vote_type   = req.body["vote_type"]
        let google_profile = req.body["gprof"]

        // on del un autre vote qu'il aurait pu faire
        await vote.deleteMany({"gprof.googleId": google_profile["googleId"], "vote_id": lamoulie_id })

        await vote.insertOne({
            gprof: google_profile,
            vote_id: lamoulie_id,
            vote: (vote_type == 1) ? 1 : -1
        })

        res.json({status: "vote_ok"})
    })

    app.post("/user", is_google_token_valid, async function(req, res) {
      let google_profile = req.body["gprof"]
      let data = await vote.find({"gprof.googleId": google_profile["googleId"]}).toArray()

      res.json(data)
    })
    
    app.get("/lamoulie", async function(req, res) {
        let data = await lamoulie.aggregate([
            {
              '$lookup': {
                'from': 'vote', 
                'localField': 'id', 
                'foreignField': 'vote_id', 
                'as': 'votes'
              }
            }, {
              '$unwind': {
                'path': '$votes'
              }
            }, {
              '$project': {
                '_id': 1, 
                'title': 1, 
                'id': 1, 
                'gprof': 1, 
                'vote_pos': {
                  '$cond': [
                    {
                      '$gt': [
                        '$votes.vote', 0
                      ]
                    }, 1, 0
                  ]
                }, 
                'vote_neg': {
                  '$cond': [
                    {
                      '$lt': [
                        '$votes.vote', 0
                      ]
                    }, 1, 0
                  ]
                }
              }
            }, {
              '$group': {
                '_id': {
                  '_id': '$_id', 
                  'title': '$title', 
                  'id': '$id'
                }, 
                'votes_p': {
                  '$sum': '$vote_pos'
                }, 
                'votes_n': {
                  '$sum': '$vote_neg'
                }
              }
            }, {
              '$project': {
                '_id': '$_id._id', 
                'title': '$_id.title', 
                'id': '$_id.id', 
                'votes_p': '$votes_p', 
                'votes_n': '$votes_n'
              }
            }
          ]).toArray()

        res.json(data)
    });
})

app.listen(8080, async (e) => {
    if (e) {
      throw e
    }
    console.log("running")
})
